package com.pwkr.AniResc.entities;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class FindAnimal {


    @Id
    @GeneratedValue
    private int id;

    String species;
    String race;
    String coloring;
    String particularFeatures;
    String currentLocalization;
    String localizationLink;
    String status;
}
