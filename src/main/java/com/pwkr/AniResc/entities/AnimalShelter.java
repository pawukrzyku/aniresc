package com.pwkr.AniResc.entities;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class AnimalShelter {
    @Id
    private int id;

    String shelterName;
    String address;
    String phoneNumber;
    String localizationLink;
}
