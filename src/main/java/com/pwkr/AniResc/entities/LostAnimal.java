package com.pwkr.AniResc.entities;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class LostAnimal {

    @Id
    @GeneratedValue
    private int id;

    String species;
    String race;
    String coloring;
    String particularFeatures;
    String lastKnowLocalization;
    String localizationLink;
    String status;

}
