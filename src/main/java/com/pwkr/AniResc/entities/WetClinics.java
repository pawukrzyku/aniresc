package com.pwkr.AniResc.entities;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class WetClinics {
    @Id
    private int id;

    String clinicName;
    String address;
    String phoneNumber;
    String localizationLink;
}
